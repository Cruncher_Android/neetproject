import { TestBed } from '@angular/core/testing';

import { SunjectsNameService } from './subjects-name.service';

describe('SunjectsNameService', () => {
  let service: SunjectsNameService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SunjectsNameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
