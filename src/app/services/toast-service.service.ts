import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastServiceService {

  constructor(private toastController: ToastController) { }

  async createToast(message) {
    let toast = await this.toastController.create({
      animated: true,
      duration: 3000,
      position: "bottom",
      message: message
    })
    await toast.present();
  }
}
