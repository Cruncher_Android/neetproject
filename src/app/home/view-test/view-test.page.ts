import { Storage } from "@ionic/storage";
import { Router } from "@angular/router";
import { DatabaseServiceService } from "./../../services/database-service.service";
import { SaveTestInfoService } from "./../../services/save-test-info.service";
import { Component, OnInit, ViewChild } from "@angular/core";

@Component({
  selector: "app-view-test",
  templateUrl: "./view-test.page.html",
  styleUrls: ["./view-test.page.scss"],
})
export class ViewTestPage implements OnInit {
  testDetails: {
    testId: number;
    testDate: string;
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
    testSubmitted: string;
  }[] = [];

  loggedInType: string = "";

  constructor(
    private saveTestInfoService: SaveTestInfoService,
    private databaseServiceService: DatabaseServiceService,
    private storage: Storage,
    private router: Router
  ) {
    // console.log('view-test page loaded');
  }

  ngOnInit() {
    // console.log('in ng on init view test')
  }

  ionViewDidEnter() {
    this.databaseServiceService.getDatabaseState().subscribe((ready) => {
      if (ready) {
        this.storage.get("userLoggedIn").then((data) => {
          this.loggedInType = data;
          this.getTest();
        });
      }
    });
  }

  getTest() {
    this.testDetails = [];
    // console.log('in get test page')
    this.saveTestInfoService
      .getTest(
        this.loggedInType == "demo" ? "save_test_info_demo" : "save_test_info"
      )
      .subscribe((data) => {
        this.testDetails = data;
        // console.log('test in page', this.testDetails);
      });
  }

  onTestClick(testSubmitted) {
    if (testSubmitted == "true") {
      this.storage.set("page", "saved-test");
    } else if (testSubmitted == "false") {
      this.storage.set("page", "paused-test");
    }
  }
}
