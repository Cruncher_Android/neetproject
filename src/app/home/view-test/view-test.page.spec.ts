import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewTestPage } from './view-test.page';

describe('ViewTestPage', () => {
  let component: ViewTestPage;
  let fixture: ComponentFixture<ViewTestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTestPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewTestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
