import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TestSeriesPage } from './test-series.page';

describe('TestSeriesPage', () => {
  let component: TestSeriesPage;
  let fixture: ComponentFixture<TestSeriesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestSeriesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TestSeriesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
