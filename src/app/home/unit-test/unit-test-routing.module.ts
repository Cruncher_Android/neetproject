import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnitTestPage } from './unit-test.page';

const routes: Routes = [
  {
    path: '',
    component: UnitTestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnitTestPageRoutingModule {}
