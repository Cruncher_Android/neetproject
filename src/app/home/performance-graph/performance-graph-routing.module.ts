import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PerformanceGraphPage } from './performance-graph.page';

const routes: Routes = [
  {
    path: '',
    component: PerformanceGraphPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PerformanceGraphPageRoutingModule {}
