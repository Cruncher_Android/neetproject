import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-chapter-list',
  templateUrl: './chapter-list.component.html',
  styleUrls: ['./chapter-list.component.scss'],
})
export class ChapterListComponent implements OnInit {

  @Input() subjectName: string;
  @Input() chapterNames = [];
  @Input() withQuestionCount: boolean;
  phy: boolean;
  chem: boolean;
  biology: boolean;
  constructor() { }

  ngOnInit() {
    for(let i = 0; i < this.chapterNames.length; i++) {
      if(this.chapterNames[i].subjectId == 1) {
        this.phy = true;
      } else if(this.chapterNames[i].subjectId == 2) {
        this.chem = true;
      } else if(this.chapterNames[i].subjectId == 4) {
        this.biology = true;
      }
    }
  }

}
